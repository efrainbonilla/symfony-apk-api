<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Observacion;
use AppBundle\Form\ObservacionType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View as FOSView;
use Hateoas\Configuration\Route;
use Hateoas\Representation\Factory\PagerfantaFactory;
use Hateoas\Representation\PaginatedRepresentation;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * Observacion controller.
 *
 * @RouteResource("Observacion")
 */
class ObservacionRESTController extends VoryxController
{
    /**
     * Get a Observacion entity
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get a Observacion entity.",
     *   section = "Observacions",
     *   output = {
     *      "class" = "AppBundle\Entity\Observacion",
     *      "groups" = {"Default"}
     *   },
     *   requirements = {
     *       {"name" = "entity", "dataType" = "Integer", "requirement" = "\d+", "description" = "Observacion ID."},
     *   },
     *   statusCodes = {
     *     200 = "Observacion's object.",
     *     404 = "Not Found."
     *   }
     * )
     *
     * @View(
     *   serializerGroups={"Default"},
     *   serializerEnableMaxDepthChecks=true
     * )
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response|Observacion
     *
     */
    public function getAction(Request $request, Observacion $entity)
    {
        return $entity;
    }

    /**
     * Get all Observacion entities.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Get all Observacion entities.",
     *   section = "Observacions",
     *   output = {
     *      "class" = "AppBundle\Entity\Observacion",
     *      "groups" = {"Default"}
     *   },
     *   statusCodes = {
     *     200 = "List of Observacion",
     *     204 = "No content. Nothing to list."
     *   }
     * )
     *
     * @View(
     *   serializerGroups={"Default"},
     *   serializerEnableMaxDepthChecks=true
     * )
     *
     * @param Request $request
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return FOSView|Response|PaginatedRepresentation
     *
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page to return.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many entities to return.")
     * @QueryParam(name="order_by", nullable=true, map=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, map=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        try {
            $page = $paramFetcher->get('page');
            $limit = $paramFetcher->get('limit');
            $order_by = !is_null($paramFetcher->get('order_by')) ? $paramFetcher->get('order_by') : array();
            $filters = !is_null($paramFetcher->get('filters')) ? $paramFetcher->get('filters') : array();

            $em = $this->getDoctrine()->getManager();
            $pager = $em->getRepository('AppBundle:Observacion')->findPaginated($filters, $order_by, $limit, $page);

            if ($pager) {
                $routeName           = $request->get('_route');
                $pagerfantaFactory   = new PagerfantaFactory(); // you can pass the page, and limit parameters name
                $paginatedCollection = $pagerfantaFactory->createRepresentation(
                    $pager,
                    new Route($routeName, array())
                );

                return $paginatedCollection;
            }
            return FOSView::create('Not Found', Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a Observacion entity.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Create a Observacion entity.",
     *   section = "Observacions",
     *   input = {
     *      "class" = "AppBundle\Form\ObservacionType",
     *      "name" = ""
     *   },
     *   output = {
     *      "class" = "AppBundle\Entity\Observacion",
     *      "groups" = {"Default"}
     *   },
     *   statusCodes = {
     *     201 = "Created object.",
     *     400 = "Bad Request. Verify your params.",
     *     500 = "Form has errors."
     *   }
     * )
     *
     * @View(
     *   serializerGroups={"Default"},
     *   statusCode=201,
     *   serializerEnableMaxDepthChecks=true
     * )
     *
     * @param Request $request
     *
     * @return FOSView|Response|Observacion
     *
     */
    public function postAction(Request $request)
    {
        $entity = new Observacion();
        $form = $this->createForm(get_class(new ObservacionType()), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $entity;
        }

        return FOSView::create(array('errors' => $form->getErrors()), Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Update a Observacion entity.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update to a Observacion entity.",
     *   section = "Observacions",
     *   input = {
     *      "class" = "AppBundle\Form\ObservacionType",
     *      "name" = ""
     *   },
     *   output = {
     *      "class" = "AppBundle\Entity\Observacion",
     *      "groups" = {"Default"}
     *   },
     *   statusCodes = {
     *     200 = "Updated object.",
     *     400 = "Bad Request. Verify your params.",
     *     404 = "Not Found.",
     *     500 = "Form has errors."
     *   }
     * )
     *
     * @View(
     *   serializerGroups={"Default"},
     *   serializerEnableMaxDepthChecks=true
     * )
     *
     * @param Request $request
     * @param $entity
     *
     * @return FOSView|Response|Observacion
     */
    public function putAction(Request $request, Observacion $entity)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(get_class(new ObservacionType()), $entity, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return $entity;
            }

            return FOSView::create(array('errors' => $form->getErrors()), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Partial Update to a Observacion entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function patchAction(Request $request, Observacion $entity)
    {
        return $this->putAction($request, $entity);
    }

    /**
     * Delete a Observacion entity.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Delete a Observacion entity.",
     *   section = "Observacions",
     *   statusCodes = {
     *     204 = "No content. Successfully excluded.",
     *     404 = "Not Found.",
     *     500 = "Internal error."
     *   }
     * )
     *
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     *
     * @return FOSView|Response
     */
    public function deleteAction(Request $request, Observacion $entity)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return null;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
