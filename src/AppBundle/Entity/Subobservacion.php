<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subobservacion
 *
 * @ORM\Table(name="subobservacion", indexes={@ORM\Index(name="id_observacion", columns={"id_observacion"})}, options={"collate"="utf8_general_ci", "charset"="utf8"})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubobservacionRepository")
 */
class Subobservacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_subobservacion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_subobservacion", type="string", length=200, nullable=false)
     */
    private $nombre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     */
    private $estado;

    /**
     * @var \Observacion
     *
     * @ORM\ManyToOne(targetEntity="Observacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_observacion", referencedColumnName="id_observacion")
     * })
     */
    private $observacion;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Subobservacion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return Subobservacion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set observacion
     *
     * @param \AppBundle\Entity\Observacion $observacion
     *
     * @return Subobservacion
     */
    public function setObservacion(\AppBundle\Entity\Observacion $observacion = null)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return \AppBundle\Entity\Observacion
     */
    public function getObservacion()
    {
        return $this->observacion;
    }
}
