<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;

/**
 * Reclamo
 *
 * @ORM\Table(name="reclamo", indexes={@ORM\Index(name="id_usu", columns={"id_usu"}), @ORM\Index(name="id_observacion", columns={"id_observacion"}), @ORM\Index(name="id_subobservacion", columns={"id_subobservacion"}), @ORM\Index(name="id_distrito", columns={"id_distrito"}), @ORM\Index(name="id_zona", columns={"id_zona"})}, options={"collate"="utf8_general_ci", "charset"="utf8"})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReclamoRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "get_reclamo",
 *          parameters = {
 *              "entity" = "expr(object.getId())"
 *          }
 *      )
 * )
 */
class Reclamo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_reclamo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="latitud", type="string", length=12, nullable=false)
     */
    private $latitud;

    /**
     * @var string
     *
     * @ORM\Column(name="longitud", type="string", length=12, nullable=false)
     */
    private $longitud;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=100, nullable=false)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable=false)
     */
    private $imagen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hora", type="time", nullable=false)
     */
    private $hora;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean", nullable=false)
     */
    private $estado;

    /**
     * @var \Distrito
     *
     * @ORM\ManyToOne(targetEntity="Distrito")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_distrito", referencedColumnName="id_distrito")
     * })
     */
    private $distrito;

    /**
     * @var \Zona
     *
     * @ORM\ManyToOne(targetEntity="Zona")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_zona", referencedColumnName="id_zona")
     * })
     */
    private $zona;

    /**
     * @var \Observacion
     *
     * @ORM\ManyToOne(targetEntity="Observacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_observacion", referencedColumnName="id_observacion")
     * })
     */
    private $observacion;

    /**
     * @var \Subobservacion
     *
     * @ORM\ManyToOne(targetEntity="Subobservacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_subobservacion", referencedColumnName="id_subobservacion")
     * })
     */
    private $subobservacion;

    /**
     * @var \Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usu", referencedColumnName="id_usu")
     * })
     */
    private $usuario;


    /**
     * @Accessor("getImagenUrl")
     * @Type("string")
     */
    private $imagenUrl;

    /**
     * Hook on pre-persist operations.
     *
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->fecha = new \DateTime();
        $this->hora = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set latitud
     *
     * @param string $latitud
     *
     * @return Reclamo
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;

        return $this;
    }

    /**
     * Get latitud
     *
     * @return string
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set longitud
     *
     * @param string $longitud
     *
     * @return Reclamo
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * Get longitud
     *
     * @return string
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Reclamo
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set imagen
     *
     * @param \File $imagen
     * @return Reclamo
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Reclamo
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set hora
     *
     * @param \DateTime $hora
     *
     * @return Reclamo
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get hora
     *
     * @return \DateTime
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return Reclamo
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set distrito
     *
     * @param \AppBundle\Entity\Distrito $distrito
     *
     * @return Reclamo
     */
    public function setDistrito(\AppBundle\Entity\Distrito $distrito = null)
    {
        $this->distrito = $distrito;

        return $this;
    }

    /**
     * Get distrito
     *
     * @return \AppBundle\Entity\Distrito
     */
    public function getDistrito()
    {
        return $this->distrito;
    }

    /**
     * Set zona
     *
     * @param \AppBundle\Entity\Zona $zona
     *
     * @return Reclamo
     */
    public function setZona(\AppBundle\Entity\Zona $zona = null)
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * Get zona
     *
     * @return \AppBundle\Entity\Zona
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * Set observacion
     *
     * @param \AppBundle\Entity\Observacion $observacion
     *
     * @return Reclamo
     */
    public function setObservacion(\AppBundle\Entity\Observacion $observacion = null)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return \AppBundle\Entity\Observacion
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set subobservacion
     *
     * @param \AppBundle\Entity\Subobservacion $subobservacion
     *
     * @return Reclamo
     */
    public function setSubobservacion(\AppBundle\Entity\Subobservacion $subobservacion = null)
    {
        $this->subobservacion = $subobservacion;

        return $this;
    }

    /**
     * Get subobservacion
     *
     * @return \AppBundle\Entity\Subobservacion
     */
    public function getSubobservacion()
    {
        return $this->subobservacion;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     *
     * @return Reclamo
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Get imagenUrl.
     *
     * @return string
     */
    public function getImagenUrl()
    {
        return sprintf('%s%s', '/uploads/apk/images/', $this->getImagen());
    }
}
