<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use AppBundle\Util\Utility;

/**
 * Class EntityRepository.
 *
 * @package AppBundle\Repository
 */
class EntityRepository extends BaseEntityRepository
{
    /**
     * Finds all the resources by criteria given, and return pagination.
     * Can do ordering, limit and offset.
     *
     * @param string[] $criteria Array which contains the criteria as key value
     * @param string[] $sorting  Array which contains the sorting as key value
     * @param int      $limit    The limit
     * @param int      $page     The page
     *
     * @return array|Pagerfanta|Paginator
     */
    public function findPaginated(array $criteria = [], array $sorting = [], $limit = 10, $page = 1)
    {
        $queryBuilder = $this->createQueryBuilder('c');

        if ($limit && $page) {
            if (class_exists('Pagerfanta\Pagerfanta')) {
                return $this->paginatePagerfanta($queryBuilder->getQuery(), $page, $limit);
            }

            return $this->paginate($queryBuilder->getQuery(), $page, $limit);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Gets paginated data by Pagerfanta.
     *
     * @param $dql
     * @param int $currentPage
     * @param int $pageSize
     *
     * @return Pagerfanta
     */
    protected function paginatePagerfanta($dql, $currentPage = 1, $pageSize = 10)
    {
        $adapter = new DoctrineORMAdapter($dql);
        $pagerfanta = new Pagerfanta($adapter);

        $pagerfanta
            ->setMaxPerPage($pageSize)
            ->setCurrentPage($currentPage);

        return $pagerfanta;
    }

    /**
     * Gets paginated data.
     *
     * @param $dql
     * @param int $currentPage
     * @param int $pageSize
     *
     * @return Paginator
     */
    protected function paginate($dql, $currentPage = 1, $pageSize = 10)
    {
        $paginator = new Paginator($dql);

        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($currentPage - 1)) // set the offset
            ->setMaxResults($pageSize); // set the limit

        return $paginator;
    }
}
