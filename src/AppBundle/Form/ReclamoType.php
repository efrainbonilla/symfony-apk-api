<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReclamoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('latitud')
            ->add('longitud')
            ->add('direccion')
            ->add('imagen', FileType::class, array(
                'required' => false,
                'data_class' => null,
                'empty_data' => null
            ))
            ->add('fecha', DateTimeType::class)
            ->add('hora', TimeType::class)
            ->add('estado')
            ->add('distrito', EntityType::class, array(
                'class' => 'AppBundle:Distrito'
            ))
            ->add('zona', EntityType::class, array(
                'class' => 'AppBundle:Zona'
            ))
            ->add('observacion', EntityType::class, array(
                'class' => 'AppBundle:Observacion'
            ))
            ->add('subobservacion', EntityType::class, array(
                'class' => 'AppBundle:Subobservacion'
            ))
            ->add('usuario', EntityType::class, array(
                'class' => 'AppBundle:Usuario'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Reclamo',
            'csrf_protection' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_reclamo';
    }


}
