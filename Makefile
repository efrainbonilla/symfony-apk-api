.PHONY: app

app:
	@echo "**************************************************"
	@echo "* Command execute file makefile *"
	@echo "**************************************************"

rest_generate:
	bin/console generate:voryx:rest --document --hateoas
	# bin/console generate:doctrine:form AppBundle:Entity

engine_reverse:
	# precaution ovewrite changes in entities
	# transform structure database in class PHP entities
	# bin/console doctrine:mapping:import AppBundle annotation

	# generate getter setter in entities
	# bin/console doctrine:generate:entities AppBundle

database_create:
	bin/console doctrine:database:create

database_drop:
	bin/console doctrine:database:drop --force

schema_drop:
	bin/console doctrine:schema:drop --force

schema_create:
	bin/console doctrine:schema:create

schema_update:
	bin/console doctrine:schema:update --force

fixtures_load:
	bin/console doctrine:fixtures:load


app_import:	database_create database_import

app_install: clear database_create schema_create fixtures_load clear

app_uninstall: database_drop clear

ifeq ($(OS),Windows_NT)

database_import:
	bin/console doctrine:database:import '%cd%/../database-backup/db.sql'

clear:
	bin/console cache:clear

else

database_import:
	bin/console doctrine:database:import '$(shell pwd)/../database-backup/db.sql'

clear:
	bin/console cache:clear

	sudo chmod 777 -R var/cache
	sudo chmod 777 -R var/logs
	sudo chmod 777 -R bin/console
endif